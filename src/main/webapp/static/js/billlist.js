var billObj;
var billStatus;
//订单管理页面上点击删除按钮弹出删除框(billlist.jsp)
function deleteBill(obj) {
    $.ajax({
        type: "GET",
        url: path + "/sys/billdel",
        data: {billid: obj.attr("billid")},
        dataType: "json",
        success: function (data) {
            console.debug(data);
            billStatus=true;
            if (data.result === "true") {//删除成功：移除删除行
                cancleBtn();
                obj.parents("tr").remove();
            } else if (data.result === "false") {//删除失败
                billStatus=false;
                //alert("对不起，删除订单【"+obj.attr("billcc")+"】失败");
                changeDLGContent("对不起，删除订单【" + obj.attr("billcc") + "】失败");
            } else if (data.result === "notexist") {
                billStatus=false;
                //alert("对不起，订单【"+obj.attr("billcc")+"】不存在");
                changeDLGContent("对不起，订单【" + obj.attr("billcc") + "】不存在");
            } else if (data.result === "noPay") {
                billStatus=false;
                changeDLGContent("对不起，订单【" + obj.attr("billcc") + "】未支付，不能删除");
            }
        },
        error: function (data) {
            alert("对不起，删除失败");
        }
    });
}

function openYesOrNoDLG() {
    $('.zhezhao').css('display', 'block');
    $('#removeBi').fadeIn();
}

function cancleBtn() {
    $('.zhezhao').css('display', 'none');
    $('#removeBi').fadeOut();
}

function changeDLGContent(contentStr) {
    var p = $(".removeMain").find("p");
    p.html(contentStr);
}

$(function () {

    $('#no').click(function () {
        cancleBtn();
    });

    $('#yes').click(function () {
        if(billStatus)deleteBill(billObj);
        else cancleBtn();
    });

    $(".deleteBill").on("click", function () {
        billObj = $(this);
        changeDLGContent("你确定要删除订单【" + billObj.attr("billcc") + "】吗？");
        openYesOrNoDLG();
    });


});