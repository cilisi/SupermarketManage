package com.ksd.dao.mapper;

import com.ksd.pojo.SmbmsAddress;
import com.ksd.pojo.SmbmsAddressExample;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@Repository
public interface SmbmsAddressMapper {
    int countByExample(SmbmsAddressExample example);

    int deleteByExample(SmbmsAddressExample example);

    int insert(SmbmsAddress record);

    int insertSelective(SmbmsAddress record);

    List<SmbmsAddress> selectByExample(SmbmsAddressExample example);

    int updateByExampleSelective(@Param("record") SmbmsAddress record, @Param("example") SmbmsAddressExample example);

    int updateByExample(@Param("record") SmbmsAddress record, @Param("example") SmbmsAddressExample example);
}