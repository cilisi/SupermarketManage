package com.ksd.manage;

import com.ksd.pojo.SmbmsUser;
import com.ksd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class Main {
    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String login(Map<String, Object> map) {
        HttpServletRequest request=Sys.getRequest();
        HttpSession session = request.getSession();
        SmbmsUser user = (SmbmsUser) session.getAttribute(Key.mainUser);
        if (user != null) {
            map.put("user", user);
            return JspPath.FRAME;
        } else {
            return JspPath.LOGIN;
        }
    }

    @RequestMapping("/login")
    public String login(String userCode, String password, Map<String, Object> map) {
        HttpServletRequest request=Sys.getRequest();
        SmbmsUser user = userService.verifyUser(userCode, password);
        if (user == null) return "401.jsp";
        HttpSession session = request.getSession();
        session.setAttribute(Key.mainUser, user);
        return JspPath.FRAME;
    }
}
