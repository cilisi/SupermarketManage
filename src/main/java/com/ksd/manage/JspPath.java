package com.ksd.manage;

public class JspPath {
    public static final String FRAME="frame.jsp";
    public static final String LOGIN="login.jsp";
    public static final String ERR_401="401.jsp";
    public static final String BILL_ADD="jsp/bill/billadd.jsp";
    public static final String BILL_LIST="jsp/bill/billlist.jsp";
    public static final String BILL_MODIFY="jsp/bill/billmodify.jsp";
    public static final String BILL_VIEW="jsp/bill/billview.jsp";
    public static final String USER_ADD="jsp/user/useradd.jsp";
    public static final String USER_LIST="jsp/user/userlist.jsp";
    public static final String USER_MODIFY="jsp/user/usermodify.jsp";
    public static final String USER_VIEW="jsp/user/userview.jsp";
    public static final String PWD_MODIFY="jsp/user/pwdmodify.jsp";
    public static final String PROVIDER_ADD="jsp/pro/provideradd.jsp";
    public static final String PROVIDER_LIST="jsp/pro/providerlist.jsp";
    public static final String PROVIDER_MODIFY="jsp/pro/providermodify.jsp";
    public static final String PROVIDER_VIEW="jsp/pro/providerview.jsp";
}
