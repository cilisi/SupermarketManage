package com.ksd.service;

import com.ksd.pojo.SmbmsBill;

import java.util.List;
import java.util.Map;

public interface BillService {

    List<SmbmsBill> GetBillAllMessage();
    List<SmbmsBill> getBill(String productName,Integer isPayment,Long providerId);
    List<SmbmsBill> getBill(List<SmbmsBill> bills,Integer currentPage);
    SmbmsBill GetBillOneMessage(Long id);
    SmbmsBill VertifyOneMessage(Map<String,String> map);
    SmbmsBill AddOneBill(Map<String,String> map,Long userId);
    BillCode RemoveOneBill(Long id);
}
