package com.ksd.service.impl;

import com.ksd.dao.mapper.SmbmsRoleMapper;
import com.ksd.pojo.SmbmsRole;
import com.ksd.pojo.SmbmsRoleExample;
import com.ksd.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private SmbmsRoleMapper roleMapper;

    @Override
    public List<SmbmsRole> getAllRole() {
        return roleMapper.selectByExample(new SmbmsRoleExample());
    }

    @Override
    public SmbmsRole getRole(Long id) {
        SmbmsRoleExample example = new SmbmsRoleExample();
        SmbmsRoleExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        List<SmbmsRole> list = roleMapper.selectByExample(example);
        return list != null && list.size() != 0 ? list.get(0) : null;
    }
}
