package com.ksd.service.impl;

import com.ksd.dao.mapper.SmbmsProviderMapper;
import com.ksd.pojo.SmbmsBill;
import com.ksd.pojo.SmbmsProvider;
import com.ksd.pojo.SmbmsProviderExample;
import com.ksd.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ProviderServiceImpl implements ProviderService {
    @Autowired
    private SmbmsProviderMapper smbmsProviderMapper;

    @Override
    public List<SmbmsProvider> getAllProvider() {
        SmbmsProviderExample example = new SmbmsProviderExample();
        List<SmbmsProvider> list = smbmsProviderMapper.selectByExample(example);
        return list;
    }

    @Override
    public SmbmsProvider getOneProvider(Long id) {
        SmbmsProviderExample example = new SmbmsProviderExample();
        SmbmsProviderExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        List<SmbmsProvider> list = smbmsProviderMapper.selectByExample(example);
        return list == null || list.size() == 0 ? null : list.get(0);
    }

    @Override
    public List<SmbmsProvider> getProvider(String queryProCode) {
        SmbmsProviderExample example = new SmbmsProviderExample();
        SmbmsProviderExample.Criteria criteria = example.createCriteria();
        if (queryProCode != null && !"".equals(queryProCode))
            criteria.andProCodeLike("%" + queryProCode + "%");
        example.setOrderByClause("id desc");
        return smbmsProviderMapper.selectByExample(example);
    }

    @Override
    public List<SmbmsProvider> getProvider(List<SmbmsProvider> providers, Integer currentPage) {
        List<SmbmsProvider> result=new ArrayList<SmbmsProvider>();
        int len = 10 * currentPage;
        if (len >= providers.size()) {
            for (int i = (currentPage - 1) * 10; i < providers.size(); i++) {
                result.add(providers.get(i));
            }
            return result;
        }
        for (int i = (currentPage - 1) * 10; i < len; i++) {
            result.add(providers.get(i));
        }
        return result;
    }

    @Override
    public List<SmbmsProvider> getProvider(List<SmbmsBill> list) {
        List<SmbmsProvider> providers = new ArrayList<>();
        for (SmbmsBill item : list) {
            SmbmsProviderExample example1 = new SmbmsProviderExample();
            SmbmsProviderExample.Criteria criteria = example1.createCriteria();
            criteria.andIdEqualTo(item.getProviderId());
            List<SmbmsProvider> list1 = smbmsProviderMapper.selectByExample(example1);
            if (list != null || list.size() != 0) providers.add(list1.get(0));
        }
        return providers;
    }

    @Override
    public List<SmbmsProvider> getProvider(List<SmbmsBill> list, Long providerId) {
        if (providerId == null) return getProvider(list);
        for (SmbmsBill item : list) {
            if (providerId.equals(item.getProviderId())) {
                list.remove(item);
            }
        }
        return getProvider(list);
    }

    @Override
    public int removeOneProvider(Long providerId) {
        SmbmsProviderExample example = new SmbmsProviderExample();
        SmbmsProviderExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(providerId);
        return smbmsProviderMapper.deleteByExample(example);
    }

    @Override
    public void vertifyOneMessage(Map<String, String> map) {
        SmbmsProvider smbmsProvider = new SmbmsProvider();
        smbmsProvider.setId(Long.valueOf(map.get("proid")));
        smbmsProvider.setProCode(map.get("proCode"));
        smbmsProvider.setProName(map.get("proName"));
        smbmsProvider.setProContact(map.get("proContact"));
        smbmsProvider.setProPhone(map.get("proPhone"));
        smbmsProvider.setProAddress(map.get("proAddress"));
        smbmsProvider.setProFax(map.get("proFax"));
        smbmsProvider.setProDesc(map.get("proDesc"));
        SmbmsProviderExample example = new SmbmsProviderExample();
        SmbmsProviderExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(Long.valueOf(map.get("proid")));
        smbmsProviderMapper.updateByExample(smbmsProvider, example);
    }

    @Override
    public void addOneProvider(Map<String, String> map) {
        SmbmsProvider smbmsProvider = new SmbmsProvider();
        smbmsProvider.setProCode(map.get("proCode"));
        smbmsProvider.setProName(map.get("proName"));
        smbmsProvider.setProContact(map.get("proContact"));
        smbmsProvider.setProPhone(map.get("proPhone"));
        smbmsProvider.setProAddress(map.get("proAddress"));
        smbmsProvider.setProFax(map.get("proFax"));
        smbmsProvider.setProDesc(map.get("proDesc"));
        smbmsProvider.setCreationDate(new Date());
        smbmsProviderMapper.insert(smbmsProvider);
    }
}
