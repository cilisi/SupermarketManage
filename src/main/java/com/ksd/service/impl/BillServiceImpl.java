package com.ksd.service.impl;

import com.ksd.dao.mapper.SmbmsBillMapper;
import com.ksd.dao.mapper.SmbmsProviderMapper;
import com.ksd.manage.Sys;
import com.ksd.pojo.SmbmsBill;
import com.ksd.pojo.SmbmsBillExample;
import com.ksd.service.BillCode;
import com.ksd.service.BillService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BillServiceImpl implements BillService {
    @Autowired
    private SmbmsBillMapper smbmsBillMapper;

    @Override
    public List<SmbmsBill> GetBillAllMessage() {
        SmbmsBillExample example = new SmbmsBillExample();
        List<SmbmsBill> list = smbmsBillMapper.selectByExample(example);
        return list;
    }

    @Override
    public List<SmbmsBill> getBill(String productName, Integer isPayment, Long providerId) {
        SmbmsBillExample example = new SmbmsBillExample();
        SmbmsBillExample.Criteria criteria = example.createCriteria();
        if (productName != null && !"".equals(productName)) {
            criteria.andProductNameLike("%"+productName+"%");
        }
        if (isPayment != null && isPayment != 0) {
            criteria.andIsPaymentEqualTo(isPayment);
        }
        List<SmbmsBill> list = smbmsBillMapper.selectByExample(example);
        example.setOrderByClause("id desc");
        if (providerId == null||providerId==0) return list;
        if (list == null) return null;
        List<SmbmsBill> list1 = new ArrayList<SmbmsBill>();
        for (SmbmsBill item : list) {
            if (providerId.equals(item.getProviderId())) {
                list1.add(item);
            }
        }
        return list1;
    }

    @Override
    public List<SmbmsBill> getBill(List<SmbmsBill> bills, Integer currentPage) {
        List<SmbmsBill> result=new ArrayList<SmbmsBill>();
        int len = 10 * currentPage;
        if (len >= bills.size()) {
            for (int i = (currentPage - 1) * 10; i < bills.size(); i++) {
                result.add(bills.get(i));
            }
            return result;
        }
        for (int i = (currentPage - 1) * 10; i < len; i++) {
            result.add(bills.get(i));
        }
        return result;
    }

    @Override
    public SmbmsBill GetBillOneMessage(Long id) {
        SmbmsBillExample example = new SmbmsBillExample();
        SmbmsBillExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        List<SmbmsBill> list = smbmsBillMapper.selectByExample(example);
        return list == null || list.size() == 0 ? null : list.get(0);
    }

    @Override
    public SmbmsBill VertifyOneMessage(Map<String, String> map) {
        SmbmsBillExample example = new SmbmsBillExample();
        SmbmsBillExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(Long.valueOf(map.get("id")));
        SmbmsBill smbmsBill = GetBillOneMessage(Long.valueOf(map.get("id")));
        smbmsBill.setId(Long.valueOf(map.get("id")));
        smbmsBill.setProductName(map.get("productName"));
        smbmsBill.setProductUnit(map.get("productUnit"));
        smbmsBill.setProductCount(new BigDecimal(map.get("productCount")));
        smbmsBill.setTotalPrice(new BigDecimal(map.get("totalPrice")));
        smbmsBill.setProviderId(Long.valueOf(map.get("providerId")));
        smbmsBill.setIsPayment(Integer.valueOf(map.get("isPayment")));
        smbmsBillMapper.updateByExample(smbmsBill, example);
        return smbmsBill;
    }

    @Override
    public SmbmsBill AddOneBill(Map<String, String> map, Long userId) {
        SmbmsBillExample example = new SmbmsBillExample();
        SmbmsBill smbmsBill = new SmbmsBill();
        smbmsBill.setBillCode(map.get("billCode"));
        smbmsBill.setProductName(map.get("productName"));
        smbmsBill.setProductUnit(map.get("productUnit"));
        smbmsBill.setProductCount(new BigDecimal(map.get("productCount")));
        smbmsBill.setTotalPrice(new BigDecimal(map.get("totalPrice")));
        smbmsBill.setProviderId(Long.valueOf(map.get("providerId")));
        smbmsBill.setCreationDate(new Date());
        smbmsBill.setIsPayment(Integer.valueOf(map.get("isPayment")));
        smbmsBill.setCreatedBy(userId);
        smbmsBillMapper.insert(smbmsBill);
        return smbmsBill;
    }

    @Override
    public BillCode RemoveOneBill(Long id) {
        SmbmsBillExample example = new SmbmsBillExample();
        SmbmsBillExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        List<SmbmsBill> list=smbmsBillMapper.selectByExample(example);
        if(list==null||list.size()==0)return BillCode.NO_EXIST;
        SmbmsBill smbmsBill=list.get(0);
        int isPay=smbmsBill.getIsPayment();
        if(isPay==2){
            int len=smbmsBillMapper.deleteByExample(example);
            return len>0?BillCode.DEL_SUCCESS:BillCode.DEL_FAILURE;
        }
        return BillCode.NO_PAY;
    }
}
