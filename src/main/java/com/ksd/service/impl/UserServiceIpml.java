package com.ksd.service.impl;

import com.ksd.dao.mapper.SmbmsRoleMapper;
import com.ksd.dao.mapper.SmbmsUserMapper;
import com.ksd.manage.Sys;
import com.ksd.pojo.SmbmsRole;
import com.ksd.pojo.SmbmsRoleExample;
import com.ksd.pojo.SmbmsUser;
import com.ksd.pojo.SmbmsUserExample;
import com.ksd.service.UserCode;
import com.ksd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class UserServiceIpml implements UserService {

    @Autowired
    private SmbmsUserMapper userMapper;

    @Autowired
    private SmbmsRoleMapper roleMapper;

    public SmbmsUser verifyUser(String userCode, String password) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andUserCodeEqualTo(userCode);
        criteria.andUserPasswordEqualTo(password);
        List<SmbmsUser> list = userMapper.selectByExample(example);
        if (list == null || list.size() == 0) return null;
        return list.get(0);
    }

    public boolean isPermissionToUserCotrol(SmbmsUser user) {
        user.getUserRole();
        SmbmsRoleExample example = new SmbmsRoleExample();
        example.setOrderByClause("id");
        List<SmbmsRole> list = roleMapper.selectByExample(example);
        if (user.getUserRole().equals(list.get(0).getId())) return true;
        return false;
    }

    @Override
    public SmbmsUser getUser(Long id) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        List<SmbmsUser> list = userMapper.selectByExample(example);
        return list != null && list.size() != 0 ? list.get(0) : null;
    }

    @Override
    public List<SmbmsUser> getUser(String userName, Long queryUserRole) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        if (userName != null && !"".equals(userName)) {
            criteria.andUserNameLike("%" + userName + "%");
        }
        if (queryUserRole != null && queryUserRole != 0) {
            criteria.andUserRoleEqualTo(queryUserRole);
        }
        example.setOrderByClause("id desc");
        List<SmbmsUser> list = userMapper.selectByExample(example);
        return list;
    }

    @Override
    public List<SmbmsUser> getUser(List<SmbmsUser> users, Integer currentPage) {
        int len = 10 * currentPage;
        List<SmbmsUser> result = new ArrayList<SmbmsUser>();
        if (len >= users.size()) {
            for (int i = (currentPage - 1) * 10; i < users.size(); i++) {
                result.add(users.get(i));
            }
            return result;
        }
        for (int i = (currentPage - 1) * 10; i < len; i++) {
            result.add(users.get(i));
        }
        return result;
    }

    @Override
    public SmbmsUser addOneUser(Map<String, String> map, SmbmsUser upUser) {
        SmbmsUser smbmsUser = new SmbmsUser();
        smbmsUser.setUserCode((String) map.get("userCode"));
        smbmsUser.setUserName((String) map.get("userName"));
        smbmsUser.setUserPassword((String) map.get("userPassword"));
        smbmsUser.setGender(Integer.valueOf(map.get("gender")));
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // Parse the text version of the date.
            //We have to perform the parse method in a
            //try-catch construct in case dateStringToParse
            //does not contain a date in the format we are expecting.
            Date date = bartDateFormat.parse((String) map.get("birthday"));
            smbmsUser.setBirthday(date);
            smbmsUser.setPhone((String) map.get("phone"));
            smbmsUser.setAddress((String) map.get("address"));
            smbmsUser.setUserRole(Long.valueOf((String) map.get("userRole")));
            smbmsUser.setCreationDate(new Date());
            smbmsUser.setCreatedBy(upUser.getId());
            userMapper.insert(smbmsUser);
            return smbmsUser;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SmbmsUser changeOneUser(Map<String, String> map) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(Long.valueOf(map.get("uid")));
        SmbmsUser smbmsUser = getUser(Long.valueOf(map.get("uid")));
        smbmsUser.setUserName(map.get("userName"));
        smbmsUser.setGender(Integer.valueOf(map.get("gender")));
        smbmsUser.setBirthday(new Date(map.get("birthday")));
        smbmsUser.setPhone(map.get("phone"));
        smbmsUser.setAddress(map.get("address"));
        smbmsUser.setUserRole(Long.valueOf(map.get("userRole")));
        userMapper.updateByExample(smbmsUser, example);
        return smbmsUser;
    }

    @Override
    public UserCode removeOneUser(Long id) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        List<SmbmsUser> list = userMapper.selectByExample(example);
        int len = userMapper.deleteByExample(example);
        return len > 0 ? UserCode.DEL_SUCCESS : UserCode.DEL_FAILURE;
    }

    @Override
    public boolean verifyUserPwd(Long id, String password) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        criteria.andUserPasswordEqualTo(password);
        List<SmbmsUser> list = userMapper.selectByExample(example);
        if (list == null || list.size() == 0) return false;
        return true;
    }

    @Override
    public boolean chanagePassword(SmbmsUser user, String newPassword) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(user.getId());
        user.setUserPassword(newPassword);
        userMapper.updateByExample(user, example);
        return true;
    }

    @Override
    public boolean isExeistUserCode(String userCode) {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andUserCodeEqualTo(userCode);
        List<SmbmsUser> list = userMapper.selectByExample(example);
        return list == null || list.size() == 0 ? false : true;
    }
}
