package com.ksd.service;

public enum UserCode {
    DEL_SUCCESS("用户删除成功",1),
    DEL_FAILURE("用户删除失败",2);

    private String msg;
    private Integer code;

    UserCode(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }
}
