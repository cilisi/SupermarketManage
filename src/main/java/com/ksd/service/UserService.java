package com.ksd.service;

import com.ksd.pojo.SmbmsUser;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Map;

public interface UserService {
    SmbmsUser verifyUser(String userCode, String password);

    boolean isExeistUserCode(String userCode);

    boolean verifyUserPwd(Long id, String passwd);

    boolean chanagePassword(SmbmsUser user, String newPassword);

    boolean isPermissionToUserCotrol(SmbmsUser user);

    SmbmsUser getUser(Long id);

    List<SmbmsUser> getUser(String queryname, Long queryUserRole);

    List<SmbmsUser> getUser(List<SmbmsUser> users, Integer currentPage);

    SmbmsUser addOneUser(Map<String, String> map, SmbmsUser pupUser);

    SmbmsUser changeOneUser(Map<String, String> map);

    UserCode removeOneUser(Long id);
}
