package com.ksd.service;

import com.ksd.pojo.SmbmsBill;
import com.ksd.pojo.SmbmsProvider;

import java.util.List;
import java.util.Map;

public interface ProviderService {
    List<SmbmsProvider> getAllProvider();
    SmbmsProvider getOneProvider(Long id);
    List<SmbmsProvider> getProvider(String queryProCode);
    List<SmbmsProvider> getProvider(List<SmbmsProvider> list,Integer currentPage);
    List<SmbmsProvider> getProvider(List<SmbmsBill> listBill);
    List<SmbmsProvider> getProvider(List<SmbmsBill> listBill, Long providerId);
    int removeOneProvider(Long providerId);
    void vertifyOneMessage(Map<String,String> map);
    void addOneProvider(Map<String,String> map);
}
