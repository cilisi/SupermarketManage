package com.ksd.service;

public enum  BillCode {
    DEL_SUCCESS("删除成功",1),
    DEL_FAILURE("删除失败",2),
    NO_PAY("订单未支付",3),
    NO_EXIST("订单不存在",4);
    private String msg;
    private Integer code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    BillCode(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }

}
