package com.ksd.service;

import com.ksd.pojo.SmbmsRole;

import java.util.List;

public interface RoleService {
    List<SmbmsRole> getAllRole();
    SmbmsRole getRole(Long id);
}
