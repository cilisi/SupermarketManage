package com.ksd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermarketManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupermarketManageApplication.class, args);
    }
}
